class UserRepository
  include Lotus::Repository

  def self.by_name(name)
    query do
      where(username: name)
    end.limit(1).first
  end
end
