class PageRepository
  include Lotus::Repository

  def self.by_title(title)
    query do
      where(title: title)
    end.first
  end
end
