require 'gollum-lib'
require 'pathname'

class Precious
  attr_reader :wiki

  def initialize
    @wiki = Gollum::Wiki.new(repo_path)
  end

  def method_missing(method, *args)
    wiki.send(method, *args)
  end

  private
  def repo_path
    path = Pathname.new(File.expand_path(ENV['GOLLUM_REPO']))
    return path if path.absolute?
    root = File.expand_path(File.join(__dir__), '../..')
    File.join(root, path)
  end
end
