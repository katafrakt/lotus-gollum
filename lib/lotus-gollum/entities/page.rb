class Page
  include Lotus::Entity

  attributes :title, :content

  def content
    return @content if @content_fetched # content might be nil as well
    @content = precious.page(title)
    @content_fetched = true
  end

  private
  def precious
    @precious ||= Precious.new
  end
end
