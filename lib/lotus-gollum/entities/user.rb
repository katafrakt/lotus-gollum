require 'scrypt'

class User
  include Lotus::Entity

  attributes :username, :email, :salt, :crypted_password

  def self.create_from(params)
    salt = SCrypt::Engine.generate_salt
    crypted_password = SCrypt::Engine.hash_secret(params[:password], salt)
    #params.delete(:password)
    new(params).tap do |user|
      user.crypted_password = crypted_password
      user.salt = salt
    end
  end
end
