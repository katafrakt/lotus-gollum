Lotus::Model.migration do
  change do
    create_table :users do
      primary_key :id
      column :username,         String, null: false
      column :email,            String, null: false
      column :salt,             String, null: false
      column :crypted_password, String, null: false
    end
  end
end
