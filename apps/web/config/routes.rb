get '/pages', to: 'pages#create'
resources :pages, path: 'wiki'
get '/sign_in', to: 'sessions#new'
post '/sign_in', to: 'sessions#create'
get '/logout', to: 'sessions#destroy'

post '/registrations', to: 'registrations#create'
get '/registrations', to: 'registrations#new'
