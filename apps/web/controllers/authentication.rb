module Web
  module Authentication
    def self.included(action)
      action.class_eval do
        before :authenticate!
        expose :current_user
      end
    end

    private

    def authenticate!

    end

    def authenticated?
      !!current_user
    end

    def current_user
      @current_user ||= UserRepository.find(session[:user_id])
    end

    module NoAuth
      private
      def authenticate!
        # no-op
      end
    end

    module Require
      def authenticate!
        halt 401 unless authenticated?
      end
    end
  end
end
