require 'scrypt'

module Web::Controllers::Sessions
  class Create
    include Web::Action

    def call(params)
      user = UserRepository.by_name(params[:user]['username'])

      if user
        salt = user.salt

        if SCrypt::Engine.hash_secret(params[:user]['password'], salt) == user.crypted_password
          session[:user_id] = user.id
        end
      end
    end

    private
    def authenticate!
      # no-op
    end
  end
end
