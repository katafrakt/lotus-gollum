module Web::Controllers::Sessions
  class Destroy
    include Web::Action

    def call(params)
      session[:user_id] = nil
      redirect_to '/sign_in'
    end
  end
end
