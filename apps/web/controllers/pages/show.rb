module Web::Controllers::Pages
  class Show
    include Web::Action
    include Web::Authentication::NoAuth

    expose :page

    def call(params)
      @page = PageRepository.by_title(params[:id])
      if @page.nil?
        @page = Page.new(title: params[:id])
        self.body = Web::Views::Pages::NotFound.render(exposures)
      end
    end
  end
end
