module Web::Controllers::Pages
  class New
    include Web::Action

    expose :page

    def call(params)
      @page = Page.new(title: params[:title])
    end
  end
end
