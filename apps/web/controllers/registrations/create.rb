module Web::Controllers::Registrations
  class Create
    include Web::Action

    params do
      param :user do
        param :username, presence: true
        param :email, presence: true
        param :password, confirmation: true, presence: true
      end
    end

    def call(params)
      if params.valid?
        user = User.create_from(params[:user])
        UserRepository.create(user)
      end
    end

    private
    def authenticate!
      # no-op
    end
  end
end
